package com.itranswarp.learnjava.service.mail;

import com.itranswarp.learnjava.entity.User;
import com.itranswarp.learnjava.messge.MailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;

@Component
public class MailService {
    @Value("${smtp.username}")
    String from;

    @Autowired JavaMailSender mailSender;
    final Logger logger = LoggerFactory.getLogger(getClass());

    public void sendRegistrationMail(MailMessage mm) {
        try {
            logger.info("[send mail] sending registration mail to {}...", mm.email);
            // TODO: simulate a long-time task:
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
            logger.info("[send mail] registration mail was sent to {}.", mm.email);
//            MimeMessage mimeMessage = mailSender.createMimeMessage();
//            MimeMessageHelper mimeHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
//            mimeHelper.setFrom(from);
//            mimeHelper.setTo(user.getEmail());
//            mimeHelper.setSubject("Hello,"+user.getName());
//            String html = String.format("<p>Hi, %s,</p><p>Your account was logged in at %s</p>", user.getName(), LocalDateTime.now());
//            mimeHelper.setText(html, true);
//            mailSender.send(mimeMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
