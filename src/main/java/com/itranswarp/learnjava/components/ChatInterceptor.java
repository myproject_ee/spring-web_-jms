package com.itranswarp.learnjava.components;

import com.itranswarp.learnjava.web.UserController;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import java.util.List;

@Component
public class ChatInterceptor extends HttpSessionHandshakeInterceptor {
    public ChatInterceptor() {
        super(List.of(UserController.KEY_USER));
    }
}
