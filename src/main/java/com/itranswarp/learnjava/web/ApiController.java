package com.itranswarp.learnjava.web;

import com.itranswarp.learnjava.entity.User;
import com.itranswarp.learnjava.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

@RestController
@RequestMapping("/api")
public class ApiController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public Callable<List<User>> users() {
        return () -> {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {}
            return userService.getUsers();
        };
    }

    @GetMapping("/users/{id}")
    public DeferredResult<User> user(@PathVariable("id") long id) {
        DeferredResult<User> deferredResult = new DeferredResult<>(3000L);
        new Thread(()->{
            try {
                Thread.sleep(1000);
            } catch (Exception e) {}
            try {
                User user = userService.getUserById(id);
                deferredResult.setResult(user);
            } catch (Exception e) {
                deferredResult.setErrorResult(Map.of("error", e.getClass().getSimpleName(), "message", e.getMessage()));
            }
        }).start();
        return deferredResult;
    }

    @PostMapping("/signin")
    public Map<String, Object> signin(@RequestBody SignInRequest signinRequest) {
        try {
            User user = userService.signin(signinRequest.email, signinRequest.password);
            return Map.of("user", user);
        } catch (Exception e) {
            return Map.of("error", "SIGNIN_FAILED", "message", e.getMessage());
        }
    }

    public static class SignInRequest {
        public String email;
        public String password;
    }
}
