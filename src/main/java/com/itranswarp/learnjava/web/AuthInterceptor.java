package com.itranswarp.learnjava.web;

import com.itranswarp.learnjava.entity.User;
import com.itranswarp.learnjava.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;

@Component
@Order(2)
public class AuthInterceptor implements HandlerInterceptor {
    @Autowired
    UserService userService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("pre authenticate {}...", request.getRequestURI());
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith("Basic ")) {
            String email_and_password = authHeader.substring(6);
            int index = email_and_password.indexOf(":");
            String email = URLDecoder.decode(email_and_password.substring(0, index), "UTF-8");
            String password = URLDecoder.decode(email_and_password.substring(index + 1), "UTF-8");
            User user = userService.signin(email, password);
            request.getSession().setAttribute(UserController.KEY_USER, user);
            logger.info("user {} login by authorization header ok.", email);
        }
        return true;
    }

}
