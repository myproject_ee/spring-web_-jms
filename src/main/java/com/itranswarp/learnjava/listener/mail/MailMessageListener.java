package com.itranswarp.learnjava.listener.mail;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itranswarp.learnjava.messge.MailMessage;
import com.itranswarp.learnjava.service.mail.MailService;
import com.itranswarp.learnjava.service.mail.MessagingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class MailMessageListener {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired ObjectMapper objectMapper;
    @Autowired MailService mailService;

    @JmsListener(destination = "jms/queue/mail", concurrency = "10")
    public void onMailMessageReceived(Message message) throws Exception {
        logger.info("received message: " + message);
        if (message instanceof TextMessage) {
            String text = ((TextMessage) message).getText();
            MailMessage mailMessage = objectMapper.readValue(text, MailMessage.class);
            mailService.sendRegistrationMail(mailMessage);
        } else {
            logger.error("unable to process non-text message!");
        }
    }
}
